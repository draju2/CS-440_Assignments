﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cs440Problem2
{
    class Program
    {
        static void Main(string[] args)
        {
            //PlayGame1();
            //PlayGame2();
            Stopwatch sw = new Stopwatch();
            sw.Start();
            PlayminimaxGame();
            sw.Stop();
            Console.WriteLine();
            Console.WriteLine(sw.Elapsed.Minutes);
            // Play game 3 is used as a test for the new changes to the game class
            //PlayGame3();
            Console.ReadKey();
        }

        static void PlayGame1()
        {
            Game game = new Game(4);

            int count = 0;
            int player = 1;

            while (count < 100 && game.GetWinner() == 0)
            {
                LinkedList<Tuple<int, int, int, int>> list = game.GetValidMovesList();

                Tuple<int, int, int, int> tMove = list.First.Value;

                if (game.PerformMove(tMove, player))
                    player *= -1;

                count++;

                game.DisplayBoard();
                Console.WriteLine();
            }
        }

        static void PlayGame2()
        {
            Game game = new Game(8);

            LinkedList<Tuple<int, int, int, int>> moves = new LinkedList<Tuple<int, int, int, int>>();
            game.DisplayBoard();
            moves.AddFirst(Tuple.Create<int, int, int, int>(1, 0, 2, 0));
            moves.AddLast(Tuple.Create<int, int, int, int>(6, 7, 5, 7));
            moves.AddLast(Tuple.Create<int, int, int, int>(2, 0, 3, 0));
            moves.AddLast(Tuple.Create<int, int, int, int>(5, 7, 4, 7));
            moves.AddLast(Tuple.Create<int, int, int, int>(3, 0, 4, 0));
            moves.AddLast(Tuple.Create<int, int, int, int>(4, 7, 3, 7));
            moves.AddLast(Tuple.Create<int, int, int, int>(4, 0, 5, 1));
            moves.AddLast(Tuple.Create<int, int, int, int>(3, 7, 2, 6));
            moves.AddLast(Tuple.Create<int, int, int, int>(5, 1, 6, 2));
            moves.AddLast(Tuple.Create<int, int, int, int>(2, 6, 1, 5));
            moves.AddLast(Tuple.Create<int, int, int, int>(1, 1, 2, 0));
            moves.AddLast(Tuple.Create<int, int, int, int>(1, 5, 0, 4));

            bool bMovesPassed = game.PerformMoves(moves);

            int iWinner = game.GetWinner();

            game.DisplayBoard();
        }

        static void PlayminimaxGame()
        {
            // Runs quickly for n = 3 takes a long time for n=4
            int n = 8;
            Game game = new Game(n);

            int player = 1;
            // This is the list of moves that will help us keep track of the game
            LinkedList<Tuple<int, int, int, int>> moves = new LinkedList<Tuple<int, int, int, int>>();

            // I am not sure if there are cases where neither opponent will have a valid move but this will
            // Check for it
            while (game.GetWinner() == 0 && game.GetValidMovesList().Count != 0)
            {
                // sets up the game board
                game.PerformMoves(moves);

                LinkedList<Tuple<int, int, int, int>> list = game.GetValidMovesList();

                // Copying the gameboard I think actually ends up cheaper. We neednt keep track of moves in
                // minimax. The only requirement is to keep track of moves in the overarching game
                int[,] game_board = (int[,])game.m_Board.Clone();

                // get best move looks through the moves and picks one with the maximum utility
                // TODO Some way to break ties right now it just picks the first it sees
                Tuple<int, int, int, int> move = getbestmove(game_board, list, player, true, true);

                if (game.PerformMove(move, player))
                {
                    // Running the game and updating the list of moves made so far
                    player *= -1;

                    if (moves.Count == 0)
                        moves.AddFirst(move);
                    else
                        moves.AddLast(move);

                    Console.Write("\n");

                    Console.Write("%d, %d, %d, %d", move.Item1, move.Item2, move.Item3, move.Item4);

                    game.DisplayBoard();

                }

                Console.WriteLine();
            }

            Console.WriteLine("FINISHED");
        }

        // minimax assigns utility to a move given the game-board
        static int minimax(int[,] game_board, Tuple<int, int, int, int> move, int player)
        {
            // see new constructor
            Game game = new Game(game_board,player);
            // if node is terminal return utility
            if (game.GetWinner() != 0)
                return -1 * game.PlayersTurn;

            // else run the move 
            else
            {
                game.PerformMove(move, game.PlayersTurn);
                LinkedList<Tuple<int, int, int, int>> moves = game.GetValidMovesList();
                // If we run out of moves for a player (This is typically when one player has no pieces left)
                // it becomes a terminal node

                // Note consider adding in a check here for endgame. It may increase efficiency
                if (moves.Count == 0)
                    return -1 * game.PlayersTurn;
                int[,] game_board_copy = (int[,])game.m_Board.Clone();
                int player_copy = game.PlayersTurn;

                // Checking which player it is black-> minimize utility otherwise maximise utility
                // This next code is equivalent to min_action(minimax(Succ(node,action))) refer sildes
                if (player_copy == -1)
                {
                    int minimum_value = int.MaxValue;
                    foreach (Tuple<int, int, int, int> tmoves in moves)
                    {
                        int utility = minimax(game_board_copy, tmoves, player_copy);
                        if (utility < minimum_value)
                        {
                            minimum_value = utility;
                        }
                    }
                    return minimum_value;
                }
                // This next code is equivalent to max_action(minimax(Succ(node,action))) refer sildes
                else
                {
                    int maximum_value = int.MinValue;

                    foreach (Tuple<int, int, int, int> tmoves in moves)
                    {
                        int utility = minimax(game_board_copy, tmoves, player_copy);
                        if (utility > maximum_value)
                        {
                            move = tmoves;
                            maximum_value = utility;
                        }
                    }
                    return maximum_value;
                }
            }
        }

        static int evalFunction(int incentivizeWeight,int penalizeWeight,Tuple<int,int,int,int> move,Game game,int player)
        {
            int categoryOfMove = game.CategoryOfMove(move, player);
            if (categoryOfMove == 3)
            {
                return incentivizeWeight * 20;
            }
            int capture = categoryOfMove == 2 ? 10 : 0;
            int threatened = categoryOfMove == 0 ? -10 : 0;
            int result = incentivizeWeight * capture + penalizeWeight * threatened;
            return result;
        }

        static int offensiveEval(Tuple<int, int, int, int> move, Game game, int player)
        {
            return evalFunction(5, 2, move, game, player);
        }

        static int defensiveEval(Tuple<int, int, int, int> move, Game game, int player)
        {
            return evalFunction(2, 5, move, game, player);
        }

        static int evalWrapper(int player,bool isWhiteOffensive,bool isBlackOffensive,Tuple<int, int, int, int> move, Game game)
        {
            if ((player == -1 && isBlackOffensive)|| (player == 1 && isWhiteOffensive))
            {
                return offensiveEval(move, game, player);
            }
            return defensiveEval(move, game, player);
        }

        // minimax assigns utility to a move given the game-board
        static int minimaxWithDepth(int[,] game_board, Tuple<int, int, int, int> move, int player, int currentDepth, int maxDepth, bool isWhiteOffensive, bool isBlackOffensive)
        {
            // see new constructor
            Game game = new Game(game_board, player);

            /*
            // if node is terminal return utility
            if (game.GetWinner() != 0)
                return -1 * game.PlayersTurn;
            */
            if (currentDepth == maxDepth)
            {
                return evalWrapper(player,isWhiteOffensive,isBlackOffensive,move,game);
            }

            // else run the move 
            else
            {
                game.PerformMove(move, game.PlayersTurn);
                LinkedList<Tuple<int, int, int, int>> moves = game.GetValidMovesList();
                // If we run out of moves for a player (This is typically when one player has no pieces left)
                // it becomes a terminal node

                // Note consider adding in a check here for endgame. It may increase efficiency
                if (moves.Count == 0)
                    return -1 * game.PlayersTurn;
                int[,] game_board_copy = (int[,])game.m_Board.Clone();
                int player_copy = game.PlayersTurn;

                // Checking which player it is black-> minimize utility otherwise maximise utility
                // This next code is equivalent to min_action(minimax(Succ(node,action))) refer sildes
                if (player_copy == -1)
                {
                    int minimum_value = int.MaxValue;
                    foreach (Tuple<int, int, int, int> tmoves in moves)
                    {
                        int utility = minimaxWithDepth(game_board_copy, tmoves, player_copy, currentDepth+1, maxDepth, isWhiteOffensive, isBlackOffensive);
                        if (utility < minimum_value)
                        {
                            minimum_value = utility;
                        }
                    }
                    return minimum_value;
                }
                // This next code is equivalent to max_action(minimax(Succ(node,action))) refer sildes
                else
                {
                    int maximum_value = int.MinValue;

                    foreach (Tuple<int, int, int, int> tmoves in moves)
                    {
                        int utility = minimaxWithDepth(game_board_copy, tmoves, player_copy, currentDepth+1, maxDepth, isWhiteOffensive, isBlackOffensive);
                        if (utility > maximum_value)
                        {
                            move = tmoves;
                            maximum_value = utility;
                        }
                    }
                    return maximum_value;
                }
            }
        }

        // alphabeta assigns utility to a move given the game-board
        static int alphaBeta(int[,] game_board, Tuple<int, int, int, int> move, int player,int alpha,int beta)
        {
            // see new constructor
            Game game = new Game(game_board, player);
            // if node is terminal return utility
            if (game.GetWinner() != 0)
                return -1 * game.PlayersTurn;

            // else run the move 
            else
            {
                game.PerformMove(move, game.PlayersTurn);
                LinkedList<Tuple<int, int, int, int>> moves = game.GetValidMovesList();
                // If we run out of moves for a player (This is typically when one player has no pieces left)
                // it becomes a terminal node

                // Note consider adding in a check here for endgame. It may increase efficiency
                if (moves.Count == 0)
                    return -1 * game.PlayersTurn;
                int[,] game_board_copy = (int[,])game.m_Board.Clone();
                int player_copy = game.PlayersTurn;

                // Checking which player it is black-> minimize utility otherwise maximise utility
                // This next code is equivalent to min_action(minimax(Succ(node,action))) refer sildes
                if (player_copy == -1)
                {
                    int v = int.MaxValue;
                    foreach (Tuple<int, int, int, int> tmoves in moves)
                    {
                        v = Math.Min(v, alphaBeta(game_board_copy, tmoves, player_copy, alpha, beta));
                        if (v <= alpha)
                        {
                            return v;
                        }
                        beta = Math.Min(beta, v);
                    }
                    return v;
                }
                // This next code is equivalent to max_action(minimax(Succ(node,action))) refer sildes
                else
                {
                    int v = int.MinValue;
                    foreach (Tuple<int, int, int, int> tmoves in moves)
                    {
                        v = Math.Max(v, alphaBeta(game_board_copy, tmoves, player_copy, alpha, beta));
                        if (v >= beta)
                        {
                            return v;
                        }
                        alpha = Math.Max(alpha, v);
                    }
                    return v;
                    
                }
            }
        }

        // This function basically is a wrapper for the initial minimax. It is required because we return the utility of
        // a state as well as a move as opposed to minimax that returns only a utility
        static Tuple<int, int, int, int> getbestmove(int[,] game_board, LinkedList<Tuple<int, int, int, int>> moves, int player, bool pruningOn, bool depthOn)
        {
            Game game = new Game(game_board,player);
            Tuple<int, int, int, int> move = new Tuple<int, int, int, int>(-1,-1,-1,-1);
            if (game.PlayersTurn == -1)
            {
                int minimum_value = int.MaxValue;
                foreach (Tuple<int, int, int, int> tmoves in moves)
                {
                    //TOCHANGE:4 cases total
                    int utility = pruningOn&&depthOn ? minimaxWithDepth((int[,])game_board.Clone(), tmoves, player, 0, 3, true, true):alphaBeta((int[,])game_board.Clone(), tmoves, player, int.MinValue, int.MaxValue);
                    if (utility < minimum_value)
                    {
                        move = tmoves;
                        minimum_value = utility;
                    }
                }
            }
            else
            {
                int maximum_value = int.MinValue;

                foreach (Tuple<int, int, int, int> tmoves in moves)
                {
                    int utility = pruningOn&&depthOn ? minimaxWithDepth((int[,])game_board.Clone(), tmoves, player, 0, 3, true, true) : alphaBeta((int[,])game_board.Clone(), tmoves, player, int.MinValue, int.MaxValue);
                    if (utility > maximum_value)
                    {
                        move = tmoves;
                        maximum_value = utility;
                    }
                }
            }
            return move;
        }

        // Testing changes to class. Feel free to ignore
        static void PlayGame3()
        {
            int m_BoardSize = 8;
            int[,] m_Board = new int[m_BoardSize, m_BoardSize];
            for (int i = 0; i < m_BoardSize; i++)
            {
                for (int j = 0; j < m_BoardSize; j++)
                {
                    m_Board[i, j] = 0;
                }
            }

            // Sets players in starting position

            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < m_BoardSize; j++)
                {
                    m_Board[i, j] = 1; // white player
                }
            }

            for (int i = m_BoardSize - 2; i < m_BoardSize; i++)
            {
                for (int j = 0; j < m_BoardSize; j++)
                {
                    m_Board[i, j] = -1; // black player
                }
            }
            Game game = new Game(m_Board, 1);
            m_Board[1, 0] = 0;

            LinkedList<Tuple<int, int, int, int>> moves = new LinkedList<Tuple<int, int, int, int>>();
            game.DisplayBoard();
            moves.AddFirst(Tuple.Create<int, int, int, int>(1, 0, 2, 0));
            moves.AddLast(Tuple.Create<int, int, int, int>(6, 7, 5, 7));
            moves.AddLast(Tuple.Create<int, int, int, int>(2, 0, 3, 0));
            moves.AddLast(Tuple.Create<int, int, int, int>(5, 7, 4, 7));
            moves.AddLast(Tuple.Create<int, int, int, int>(3, 0, 4, 0));
            moves.AddLast(Tuple.Create<int, int, int, int>(4, 7, 3, 7));
            moves.AddLast(Tuple.Create<int, int, int, int>(4, 0, 5, 1));
            moves.AddLast(Tuple.Create<int, int, int, int>(3, 7, 2, 6));
            moves.AddLast(Tuple.Create<int, int, int, int>(5, 1, 6, 2));
            moves.AddLast(Tuple.Create<int, int, int, int>(2, 6, 1, 5));
            moves.AddLast(Tuple.Create<int, int, int, int>(1, 1, 2, 0));
            moves.AddLast(Tuple.Create<int, int, int, int>(1, 5, 0, 4));

            bool bMovesPassed = game.PerformMoves(moves);

            int iWinner = game.GetWinner();

            game.DisplayBoard();


        }
    }
}
