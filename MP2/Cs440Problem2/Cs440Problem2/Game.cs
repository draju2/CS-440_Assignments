﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cs440Problem2
{
    class Game
    {
        // -1 is black
        // 1 is white
        public int[,] m_Board;
        int m_BoardSize;

        public Game(int boardSize)
        {
            m_BoardSize = boardSize;
            m_Board = new int[m_BoardSize, m_BoardSize];
            ResetBoard();
        }

        public Game(int[,] board, int turn)
        {
            m_BoardSize = board.GetLength(0);
            m_Board = (int[,])board.Clone();
            PlayersTurn = turn;
        }

        public int PlayersTurn
        {
            get; private set;
        }


        public void ResetBoard()
        {
            // black goes first
            this.PlayersTurn = 1;

            // Resets board to all zeros
            for (int i = 0; i < m_BoardSize; i++)
            {
                for (int j = 0; j < m_BoardSize; j++)
                {
                    m_Board[i, j] = 0;
                }
            }

            // Sets players in starting position
            if (m_BoardSize >= 8)
            {
                SetPlayers(2);
            }
            else
            {
                SetPlayers(1);
            }
        }

        private void SetPlayers(int iRows)
        {
            for (int i = 0; i < iRows; i++)
            {
                for (int j = 0; j < m_BoardSize; j++)
                {
                    m_Board[i, j] = 1; // white player
                }
            }

            for (int i = m_BoardSize - iRows; i < m_BoardSize; i++)
            {
                for (int j = 0; j < m_BoardSize; j++)
                {
                    m_Board[i, j] = -1; // black player
                }
            }
        }

        public bool PerformMoves(LinkedList<Tuple<int, int, int, int>> moves)
        {
            foreach (Tuple<int, int, int, int> tMove in moves)
            {
                if (!PerformMove(tMove, this.PlayersTurn))
                    return false;
            }

            return true;
        }

        public bool PerformMove(Tuple<int, int, int, int> move, int player)
        {
            if (this.PlayersTurn != player)
                return false;

            // from i,j to i,j
            if (!IsValidMove(move, player))
                return false;

            m_Board[move.Item1, move.Item2] = 0;
            m_Board[move.Item3, move.Item4] = player;
            // flip players trun
            this.PlayersTurn = this.PlayersTurn * -1;

            return true;
        }

        public int GetWinner()
        {
            if (IsBlackWinner())
                return -1;
            else if (IsWhiteWinner())
                return 1;
            else
                return 0;
        }

        private bool IsBlackWinner()
        {
            // black in first row
            for (int j = 0; j < m_BoardSize; j++)
            {
                if (m_Board[0, j] == -1)
                    return true;
            }

            // check if white exist
            bool bWhiteExist = false;

            for (int i = 0; i < m_BoardSize; i++)
            {
                for (int j = 0; j < m_BoardSize; j++)
                {
                    if (m_Board[i, j] == 1)
                    {
                        bWhiteExist = true;
                        break;
                    }
                }
            }

            if (!bWhiteExist)
                return true;

            return false;
        }

        private bool IsWhiteWinner()
        {
            // white in final row
            for (int j = 0; j < m_BoardSize; j++)
            {
                if (m_Board[m_BoardSize - 1, j] == 1)
                    return true;
            }

            // check if black exist
            bool bBlackExist = false;

            for (int i = m_BoardSize - 1; i >= 0; i--)
            {
                for (int j = 0; j < m_BoardSize; j++)
                {
                    if (m_Board[i, j] == -1)
                    {
                        bBlackExist = true;
                        break;
                    }
                }
            }

            if (!bBlackExist)
                return true;

            return false;
        }

        public LinkedList<Tuple<int, int, int, int>> GetValidMovesList()
        {
            SortedDictionary<int, Tuple<int, int, int, int>> catTree; // = new SortedDictionary<int, Tuple<int, int, int, int>>();
            int player = this.PlayersTurn;

            if (player == -1)
                catTree = new SortedDictionary<int, Tuple<int, int, int, int>>(new DuplicateKeyComparerBlack<int>());
            else if (player == 1)
                catTree = new SortedDictionary<int, Tuple<int, int, int, int>>(new DuplicateKeyComparerWhite<int>());
            else
            {
                Debug.Assert(false);
                return null;
            }

            for (int i = 0; i < m_BoardSize; i++)
            {
                for (int j = 0; j < m_BoardSize; j++)
                {
                    if (m_Board[i, j] == player)
                    {
                        List<Tuple<int, int, int, int>> moves = GetValidMoveList(i, j, player);

                        foreach (Tuple<int, int, int, int> move in moves)
                        {
                            int category = CategoryOfMove(move, player);
                            catTree.Add(category, move);
                        }
                    }
                }
            }

            // populate the linked list
            LinkedList<Tuple<int, int, int, int>> ret = new LinkedList<Tuple<int, int, int, int>>();
            foreach (KeyValuePair<int, Tuple<int, int, int, int>> kv in catTree)
            {
                ret.AddFirst(kv.Value);
            }

            return ret;
        }

        private List<Tuple<int, int, int, int>> GetValidMoveList(int i, int j, int player)
        {
            List<Tuple<int, int, int, int>> ret = new List<Tuple<int, int, int, int>>();

            // move up
            Tuple<int, int, int, int> tMoveUp = new Tuple<int, int, int, int>(i, j, i + player, j);
            if (IsValidMove(tMoveUp, player))
                ret.Add(tMoveUp);

            // move diagonal right
            Tuple<int, int, int, int> tMoveRight = new Tuple<int, int, int, int>(i, j, i + player, j + 1);
            if (IsValidMove(tMoveRight, player))
                ret.Add(tMoveRight);

            // move diagonal left
            Tuple<int, int, int, int> tMoveLeft = new Tuple<int, int, int, int>(i, j, i + player, j - 1);
            if (IsValidMove(tMoveLeft, player))
                ret.Add(tMoveLeft);

            return ret;
        }

        private bool IsThreateningPosition(int i, int j, int player)
        {
            List<Tuple<int, int, int, int>> ret = new List<Tuple<int, int, int, int>>();

            // threatening diagonal right
            Tuple<int, int, int, int> tMoveRight = new Tuple<int, int, int, int>(i, j, i + player, j + 1);
            if (IsValidMoveTo(tMoveRight, player))
            {
                if (m_Board[tMoveRight.Item3, tMoveRight.Item4] == player * -1)
                    return true;
            }

            // threatening diagonal left
            Tuple<int, int, int, int> tMoveLeft = new Tuple<int, int, int, int>(i, j, i + player, j - 1);
            if (IsValidMoveTo(tMoveLeft, player))
            {
                if (m_Board[tMoveLeft.Item3, tMoveLeft.Item4] == player * -1)
                    return true;
            }

            return false;
        }

        private bool IsValidMove(Tuple<int, int, int, int> move, int player)
        {
            if (!IsValidMoveFrom(move, player))
                return false;

            if (!IsValidMoveTo(move, player))
                return false;

            return true;
        }

        private bool IsValidMoveFrom(Tuple<int, int, int, int> move, int player)
        {
            // Is the piece there
            if (m_Board[move.Item1, move.Item2] != player)
                return false;

            return true;
        }

        private bool IsValidMoveTo(Tuple<int, int, int, int> move, int player)
        {
            // is the move on the board
            if (!IsMoveOnBoard(move))
                return false;

            // is your own piece at move location
            if (m_Board[move.Item3, move.Item4] == player)
                return false;

            // is it forward or forward diagonal
            if (!IsForwardDiagonalMove(move, player))
                return false;

            return true;
        }

        private bool IsMoveOnBoard(Tuple<int, int, int, int> move)
        {
            if (move.Item1 < 0 || move.Item1 > m_BoardSize - 1)
                return false;

            if (move.Item2 < 0 || move.Item2 > m_BoardSize - 1)
                return false;

            if (move.Item3 < 0 || move.Item3 > m_BoardSize - 1)
                return false;

            if (move.Item4 < 0 || move.Item4 > m_BoardSize - 1)
                return false;

            return true;
        }

        private bool IsForwardDiagonalMove(Tuple<int, int, int, int> move, int player)
        {
            int moveup = (move.Item3 - move.Item1) * player;

            if (moveup != 1)
                return false;

            int moveSide = move.Item4 - move.Item2;

            if (moveSide == 0 && m_Board[move.Item3, move.Item4] != 0)
                return false;

            if (moveSide < -1 && moveSide > 1)
                return false;

            return true;
        }

        // win the game > capture a piece > moving the most forward piece ahead (non threatened positions) > moving into a threatened position
        public int CategoryOfMove(Tuple<int, int, int, int> move, int player)
        {
            // win the game = 3
            if (player == -1 && move.Item3 == 0)
                return 3;
            else if (player == 1 && move.Item3 == m_BoardSize - 1)
                return 3;

            // capture a piece = 2
            if (m_Board[move.Item3, move.Item4] == player * -1)
                return 2;

            // moving into a threatened position = 0
            if (IsThreateningPosition(move.Item3, move.Item4, player))
                return 0;

            return 1;
        }

        public void DisplayBoard()
        {
            StringBuilder sb = new StringBuilder();
            Console.Write("\n");
            for (int i = 0; i < m_BoardSize; i++)
            {
                for (int j = 0; j < m_BoardSize; j++)
                {
                    int value = m_Board[i, j];
                    if (value == -1)
                        sb.Append(value);
                    else
                        sb.Append(" " + value);
                }
                sb.AppendLine();
            }

            Console.Write(sb.ToString());
        }

    }


    public class DuplicateKeyComparerBlack<TKey> : IComparer<TKey> where TKey : IComparable
    {
        public int Compare(TKey x, TKey y)
        {
            int result = x.CompareTo(y);

            if (result == 0)
                return -1;   // Handle equality as beeing greater
            else
                return result;
        }
    }


    public class DuplicateKeyComparerWhite<TKey> : IComparer<TKey> where TKey : IComparable
    {
        public int Compare(TKey x, TKey y)
        {
            int result = x.CompareTo(y);

            if (result == 0)
                return 1;   // Handle equality as beeing less
            else
                return result;
        }
    }
}
