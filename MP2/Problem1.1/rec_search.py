import copy as cp
import numpy as np
import sudoku_reader as sr
import state as st
import init_step as ist

def recursive_backtracking_search(state_matrix,state_vars,unassigned,assigned,count):
    solution_state = st.state(state_matrix,state_vars,unassigned,assigned)
    if(solution_state.iscomplete()):
        print ('SOLVED')
        return (solution_state,count)

    if(solution_state.isfailure() == False):
        print ('Forward Checking Failure')
        return (False,count)

    word,position = ist.Find_next_assignment(state_matrix,state_vars,unassigned,assigned)

    while(len(state_vars[word])>0):
        print (word,position,len(unassigned),len(state_vars[word]))
        print (state_matrix)
        solution_state.assign_position(word,position)
        count = count + 1
        solution_state.check_already_assigned()
        state_matrix_1,state_vars_1,unassigned_1,assigned_1 = ist.Forward_checking(solution_state.state_matrix, solution_state.state_vars, solution_state.unassigned, solution_state.assigned) 
        
        
        result = recursive_backtracking_search(state_matrix_1,state_vars_1,unassigned_1,assigned_1,count)

        if(result[0] != False):
            return (result)

        if (result[0] == False):
            state_vars[word].remove(position)
            word,position = ist.Find_next_assignment(state_matrix,state_vars,unassigned,assigned)
            solution_state = st.state(state_matrix,state_vars,unassigned,assigned)
            count = result[1]

    print ('Backtracking')
    return (False,count)
