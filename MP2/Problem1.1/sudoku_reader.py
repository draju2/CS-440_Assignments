import numpy as np

def sudoku_file_read(filename):
    print ('reading_file')
    with open (filename, "r") as myfile:
        data=myfile.readlines()

    print('trimming')
    for i in range(len(data)):
        data[i] = data[i].replace('\r\n','')
        data[i] = data[i].replace('\n','')
        data[i] = data[i].upper()

    return data 
    
def sudoku_grid_read(filename):
    data = sudoku_file_read(filename)
    data_list = [[],[],[],[],[],[],[],[],[]]
    for i in range (len(data)):
        for j in range (len(data[i])):
            data_list[i].append(data[i][j])

    return (np.array(data_list))
