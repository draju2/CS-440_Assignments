import copy as cp
import numpy as np
import sudoku_reader as sr
import state as st
def initialise_problem(grid_name,bank_name):
    word_list = sr.sudoku_file_read(bank_name)
    state_matrix = sr.sudoku_grid_read(grid_name)
    list_all = []
    assigned = []
    for i in range (len(state_matrix)):
        for j in range (len(state_matrix[i])):
            list_all.append((i,j,'a'))
            list_all.append((i,j,'d'))

    state_vars = {}
    temp_state = st.state(state_matrix,state_vars,[],assigned)
    for i in range(len(word_list)):
        list_pos = []
        for j in range(len(list_all)):
            if(temp_state.validate_position(word_list[i],list_all[j])):
                list_pos.append(list_all[j])

        state_vars[word_list[i]] = list_pos

    return state_matrix,state_vars,word_list,assigned

def Forward_checking(state_matrix,state_vars,unassigned,assigned):
    state_vars_new = {}
    temp_state = st.state(state_matrix,state_vars,unassigned,assigned)
    for i in range (len(unassigned)):
        list_pos = []
        for j in range(len(state_vars[unassigned[i]])):
            if(temp_state.validate_position(unassigned[i],state_vars[unassigned[i]][j])):
                list_pos.append(state_vars[unassigned[i]][j])

        state_vars_new[unassigned[i]] = list_pos
    

    for i in range(len(assigned)):
        state_vars_new[assigned[i]] = state_vars[assigned[i]]
    return state_matrix,state_vars_new,unassigned,assigned

def Find_next_assignment(state_matrix,state_vars,unassigned,assigned):
    state_matrix_temp = cp.deepcopy(state_matrix)
    state_vars_temp = cp.deepcopy(state_vars)
    unassigned_temp = cp.deepcopy(unassigned)
    assigned_temp = cp.deepcopy(assigned)

    next_assign = 0
    minimum_options = float('inf')
    for i in range(len(unassigned)):
        if (len(state_vars[unassigned[i]]) < minimum_options):
            minimum_options = len(state_vars[unassigned[i]])
            next_assign = i
    
    max_options = 0
    next_position = 0
    for i in range(len(state_vars[unassigned[next_assign]])):
        temp_state = st.state(state_matrix_temp,state_vars_temp,unassigned_temp,assigned_temp)
        temp_state.assign_position(unassigned_temp[next_assign],state_vars[unassigned_temp[next_assign]][i])
        temp_state.check_already_assigned()
        temp_res = Forward_checking(temp_state.state_matrix,temp_state.state_vars,temp_state.unassigned,temp_state.assigned)
        temp_state = st.state(temp_res[0],temp_res[1],temp_res[2],temp_res[3])
        summer = 0
        for j in range(len(temp_state.unassigned)):
            summer = summer + len(temp_state.state_vars[temp_state.unassigned[j]])

        if (summer>max_options):
            max_options = summer
            next_position = i

    if(len(state_vars[unassigned[next_assign]]) == 0):
        print (unassigned[next_assign])
        return (unassigned[next_assign],[])

    return (unassigned[next_assign],state_vars[unassigned[next_assign]][next_position])
