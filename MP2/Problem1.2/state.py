import copy as cp
import numpy as np

class state:
    def __init__(self,state_matrix,state_vars,unassigned,assigned):
        self.state_matrix = cp.deepcopy(state_matrix)
        self.state_vars = cp.deepcopy(state_vars)
        self.unassigned = cp.deepcopy(unassigned)
        self.assigned = cp.deepcopy(assigned)

    def validate_position (self,word,position):
        word_len = len(word)
        if (position[2] == 'd'):
            if(position[0]+word_len>9):
                return False
        else:
            if(position[1]+word_len>9):
                return False

        if (position[2] == 'd'):
            grid_sl = self.state_matrix[position[0]:position[0]+word_len,position[1]]

        else :
            grid_sl = self.state_matrix[position[0],position[1]:position[1]+word_len]

        for i in range(word_len):
            if(( grid_sl[i] != '_') and (grid_sl[i]!= word[i])):
                return False

        if (position[2] == 'd'):
            for i in range (word_len):
                if(word[i] in self.state_matrix[0:position[0],position[1]] or word[i] in self.state_matrix[position[0]+word_len:,position[1]] ):
                    return False

        else:
            for i in range (word_len):
                if(word[i] in self.state_matrix[position[0],0:position[1]] or word[i] in self.state_matrix[position[0],position[1]+word_len:] ):
                    return False

        if (position[2] == 'd'):
            for i in range (word_len):
                if((word[i] in self.state_matrix[position[0]+i,:]) and (word[i]!= grid_sl[i])):
                    return False

        else:
            for i in range (word_len):
                if((word[i] in self.state_matrix[:,position[1]+i]) and (word[i]!= grid_sl[i])):
                    return False

        if (position[2]=='d'):
            if(position[1]<=2):
                bracketj = (0,3)
            elif (position[1]>=6):
                bracketj = (6,9)
            else:
                bracketj = (3,6)

            for i in range(word_len):
                if (position[0]+i<=2):
                    bracketi = (0,3)
                elif (position[0]+i>=6):
                    bracketi = (6,9)
                else:
                    bracketi = (3,6)
                
                if (word[i] in self.state_matrix[bracketi[0]:bracketi[1],bracketj[0]:bracketj[1]]):
                    if (word[i] != grid_sl[i]):
                        return False

        if (position[2]=='a'):
            if(position[0]<=2):
                bracketi = (0,3)
            elif (position[0]>=6):
                bracketi = (6,9)
            else:
                bracketi = (3,6)

            for i in range(word_len):
                if (position[1]+i<=2):
                    bracketj = (0,3)
                elif (position[1]+i>=6):
                    bracketj = (6,9)
                else:
                    bracketj = (3,6)
                
                if (word[i] in self.state_matrix[bracketi[0]:bracketi[1],bracketj[0]:bracketj[1]]):
                    if (word[i] != grid_sl[i]):
                        return False

        return True

    def assign_position(self,word,position):
        if(position in self.state_vars[word]):
            self.state_vars[word] = position
            self.unassigned.remove(word)
            self.assigned.append(word)
            if (position[2]=='d'):
                for i in range(len(word)):
                    self.state_matrix[position[0]+i,position[1]] = word[i]
            else:
                for i in range(len(word)):
                    self.state_matrix[position[0],position[1]+i] = word[i]

    def check_already_assigned (self):
        for j in range (len(self.state_matrix)):
            temp_checker = np.array_str(self.state_matrix[j,:]).strip('[').strip(']').replace("'","").replace(" ","")
            found = []
            for i in range(len(self.unassigned)):
                if (temp_checker.find(self.unassigned[i]) != -1 and len(self.state_vars[self.unassigned[i]])!=0):
                    position = (j,temp_checker.find(self.unassigned[i]),'a')
                    self.state_vars[self.unassigned[i]] = position
                    found = [self.unassigned[i]]+found
            for i in range (len(found)):
                self.unassigned.remove(found[i])
                self.assigned.append(found[i])
                #print ('Removed',found[i])

        for j in range (len(self.state_matrix)):
            temp_checker = np.array_str(self.state_matrix[:,j]).strip('[').strip(']').replace("'","").replace(" ","")
            found = []
            for i in range(len(self.unassigned)):
                if (temp_checker.find(self.unassigned[i]) != -1 and len(self.state_vars[self.unassigned[i]])!=0):
                    position = (temp_checker.find(self.unassigned[i]),j,'d')
                    self.state_vars[self.unassigned[i]] = position
                    found = [self.unassigned[i]]+found
            for i in range (len(found)):
                self.unassigned.remove(found[i])
                self.assigned.append(found[i])
                #print ('Removed',found[i])
    
    def iscomplete (self):
        if('_' in self.state_matrix):
            return False
        else:
            return True

#        if(not(len(self.unassigned)==0)):
#            return False
#        else:
#            return True


    def isfailure (self):
#        num = 0
#        for i in range(len(self.unassigned)):
#            if( len(self.state_vars[self.unassigned[i]]) == 0 ):
#                print (self.unassigned[i])
#                num = num +1
#                if(num>k or num==len(self.unassigned)):
#                    print (num)
#                    return True

#        print (num)

        blank = len(np.where(self.state_matrix == '_')[0])
        max_possible = 0
        for i in range(len(self.unassigned)):
            if ( len(self.state_vars[self.unassigned[i]]) != 0 ):
                max_possible = max_possible + len(self.unassigned[i])

        if (max_possible < blank):
            #print ('Backtracking')
            return True
        
        return False        
