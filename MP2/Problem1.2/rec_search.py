import copy as cp
import numpy as np
import sudoku_reader as sr
import state as st
import init_step as ist

solution_list = []

def recursive_backtracking_search(state_matrix,state_vars,unassigned,assigned,count):
    solution_state = st.state(state_matrix,state_vars,unassigned,assigned)
    if(solution_state.iscomplete()):
        print ('SOLVED')
        print ((solution_state.unassigned))
        return (True,count,solution_state)

    if(solution_state.isfailure() == True):
        #print ('Forward Checking Failure')
        #print ([solution_state.unassigned[i] for i in range(len(solution_state.unassigned)) if len(solution_state.state_vars[solution_state.unassigned[i]])==0])
        return (False,count)

    word,position = ist.Find_next_assignment(state_matrix,state_vars,unassigned,assigned)

    while(solution_state.isfailure() == False and len(position)!=0):
        #print (word,position,len(unassigned),len(state_vars[word]),len(solution_list))
        #print (state_matrix)
        solution_state.assign_position(word,position)
        count = count + 1
        solution_state.check_already_assigned()
        state_matrix_1,state_vars_1,unassigned_1,assigned_1 = ist.Forward_checking(solution_state.state_matrix, solution_state.state_vars, solution_state.unassigned, solution_state.assigned) 
        
        
        result = recursive_backtracking_search(state_matrix_1,state_vars_1,unassigned_1,assigned_1,count)

        if(result[0] != False):
            global solution_list
            solution_list = solution_list + [result[2]]
            #print (word,position)
            state_vars[word].remove(position)
            word,position = ist.Find_next_assignment(state_matrix,state_vars,unassigned,assigned)
            #print (word,position)
            count = result[1]
            print ("ADDED")

        if (result[0] == False):
            state_vars[word].remove(position)
            word,position = ist.Find_next_assignment(state_matrix,state_vars,unassigned,assigned)
            solution_state = st.state(state_matrix,state_vars,unassigned,assigned)
            count = result[1]

        if (count%1000 == 0):
            print (count)


    #print ('Backtracking')
    return (False,count)

def make_solution (filename1,filename2):
    res = ist.initialise_problem(filename1,filename2)
    global solution_list
    solution = recursive_backtracking_search(res[0],res[1],res[2],res[3],0)
    return solution_list,solution[1]
