import pprint as pp
import math
import numpy as np
import time

'''image_array is the collection of all images, 
a 3d array that contains a 28*28 2d array, 
image_labels is the class that each image belongs to'''
def readFile(imageFileName,labelFileName):
	fd_images = open(imageFileName,"r")
	fd_labels = open(labelFileName,"r")

	image_array = []
	image_elem = []
	count = 0
	for i,line in enumerate(fd_images.readlines()):
		if i != 0 and i%28 == 0:
			image_array.append(image_elem)
			image_elem = []
			count = i
		image_elem.append(line[:-1])

	image_array.append(image_elem)
	
	image_labels = [line.strip() for line in fd_labels.readlines()]

	return image_array,image_labels

def allImagesWithClassLabel(label,image_array,image_labels):
	indices = [i for i,l in enumerate(image_labels) if l==label]
	return [image_array[index] for index in indices]

def train(m,n,overlap):
	image_array,image_labels=readFile("trainingimages","traininglabels")
	totalLength=len(image_array)
	priors=[]
        if (overlap==True):
  	    conditional_prob=np.zeros((10,28-m+1,28-n+1,2**(m*n)),dtype = np.float16)
        else:
  	    conditional_prob=np.zeros((10,28/m,28/n,2**(m*n)),dtype = np.float16) 
        print (conditional_prob.shape)
	for i in range(10):
		result=allImagesWithClassLabel(str(i),image_array,image_labels)
                result_1 = []
                for j in range(len(result)):
                        result_1.append( np.array(map(list,result[j])) )
		print(i)
		#calculate prior
		priors.append((float)(len(result))/totalLength) 

		classLength=len(result)
		#calculate P(Fij|class)
                if(overlap==True):
                    for elem in result_1:
			    for x in range(28-m+1):
				    for y in range (28-n+1):
                                        val = convert_to_index(elem[x:x+m,y:y+n])
                                        conditional_prob[i,x,y,val] += 1

                else:
                    for elem in result_1:
			    for x in range(28/m):
				    for y in range (28/n):
                                        val = convert_to_index(elem[x*m:m*(x+1),y*n:n*(y+1)])
                                        conditional_prob[i,x,y,val] += 1

                k = 0.1
                conditional_prob[i,:,:,:] = (conditional_prob[i,:,:,:]+k)/(classLength+2**(m*n)*k)

#		for x in range(28-m+1):
#			for y in range(28-n+1):
#                        	for val in range (2**(m*n)):
#					conditional_prob[i,x,y,val]=sum(convert_to_index(elem[x:x+m,y:y+n])==val for elem in result_1)
				#smoothing 
#					k=5
#					conditional_prob[i,x,y,val]=(float)(conditional_prob[i,x,y,val]+1)/(classLength+k)

	return conditional_prob,priors

def chainRuleHelper(featureValue,x,y,conditional_prob,classIndex,initial):
	valueIndex = convert_to_index(featureValue)
	return (initial[classIndex]+math.log(conditional_prob[classIndex,x,y,valueIndex]))

def classify():
	image_array,image_labels=readFile("testimages","testlabels")
	start_time = time.time()
        m = 4
        n = 2
        overlap = True
	conditional_prob,priors=train(m,n,overlap)
        print (str(time.time()-start_time) + ' seconds')
	#print(conditional_prob[0][0][0])
	result=[]
	indices=range(10)
	most_prototypal_value=[float("-inf")]*10
	most_prototypal_index=[0]*10
	least_prototypal_value=[float("inf")]*10
	least_prototypal_index=[0]*10
	for j,image in enumerate(image_array):
		initial=map(math.log,priors)
                new_image = np.array(map(list,image))
		#print initial
                if (overlap==True):
    		    for x in range(28-m+1):
			    for y in range(28-n+1):
				    initial=map(lambda e:chainRuleHelper(new_image[x:x+m,y:y+n],x,y,conditional_prob,e,initial),indices)
		else:
    		    for x in range(0,28/m):
			    for y in range(0,28/n):
				    initial=map(lambda e:chainRuleHelper(new_image[x*m:m*(x+1),y*n:n*(y+1)],x,y,conditional_prob,e,initial),indices)
		    
		class_value=max(initial)
		class_result=initial.index(class_value)
		if class_value>most_prototypal_value[class_result]:
			most_prototypal_value[class_result]=class_value
			most_prototypal_index[class_result]=j
		if class_value<least_prototypal_value[class_result]:
			least_prototypal_value[class_result]=class_value
			least_prototypal_index[class_result]=j
		result.append(str(class_result))

	totalCount=len(image_labels)
	accuracy=sum(image_labels[i]==result[i] for i in range(totalCount))
	print(accuracy)
	#print(accuracy)
	#pp.pprint(result)
	accuracyPercent=(float)(accuracy)/totalCount
	print(accuracyPercent)

	confusion_matrix=[[0 for i in range(10)] for j in range(10)]
	classCount=[0]*10
	for i in range(totalCount):
		experiment_class=result[i]
		#print experiment_class
		real_class=image_labels[i]
		classCount[int(real_class)]+=1
		#print real_class
		confusion_matrix[int(real_class)][int(experiment_class)]+=1
		#pp.pprint(confusion_matrix)
		#break
	for i in range(10):
		for j in range(10):
			confusion_matrix[i][j]/=float(classCount[i])

	confusion_matrix = np.array(confusion_matrix)
	np.set_printoptions(3)
	print(confusion_matrix)
	#print(most_prototypal_value)
	#print(least_prototypal_value)

	for i in range(10):
		print "Class: {}".format(i)
		print("most_prototypal")
		pp.pprint(image_array[most_prototypal_index[i]])

		pp.pprint("least_prototypal")
		pp.pprint(image_array[least_prototypal_index[i]])
	#to do:4.draw graphs & odd ratios
	print (str(time.time()-start_time) + ' seconds')

def convert_to_index (feature):
    m,n = feature.shape
    np_feature = np.zeros((m,n))
    for i in range(m):
        for j in range(n):
            if (feature[i][j] == '#' or feature[i][j] == '+'):
                np_feature[i][j] = 1
            else:
                np_feature[i][j] = 0
    np_feature = np_feature.flatten()
    conv = 2**np.arange(len(np_feature))
    return (int(np.dot(conv,np_feature)))
        
