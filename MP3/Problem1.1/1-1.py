import pprint as pp
import math
import numpy as np
import matplotlib.pyplot as plt

def main():
	pass

'''image_array is the collection of all images, 
a 3d array that contains a 28*28 2d array, 
image_labels is the class that each image belongs to'''
def readFile(imageFileName,labelFileName):
	fd_images = open(imageFileName,"r")
	fd_labels = open(labelFileName,"r")

	image_array = []
	image_elem = []
	count = 0
	for i,line in enumerate(fd_images.readlines()):
		if i != 0 and i%28 == 0:
			image_array.append(image_elem)
			image_elem = []
			count = i
		image_elem.append(line[:-1])

	image_array.append(image_elem)
	
	image_labels = [line.strip() for line in fd_labels.readlines()]

	return image_array,image_labels

def allImagesWithClassLabel(label,image_array,image_labels):
	indices = [i for i,l in enumerate(image_labels) if l==label]
	return [image_array[index] for index in indices]

def train(k):
	image_array,image_labels=readFile("trainingimages","traininglabels")
	totalLength=len(image_array)
	priors=[]
	conditional_prob=[]
	for i in range(10):
		result=allImagesWithClassLabel(str(i),image_array,image_labels)

		#calculate prior
		priors.append((float)(len(result))/totalLength) 

		classLength=len(result)
		#calculate P(Fij|class)
		conditional_prob_elem=[]
		for x in range(28):
			conditional_pixel_elem=[]
			for y in range(28):
				foreground_count=sum(elem[x][y]=='+' or elem[x][y]=='#' for elem in result)
				background_count=sum(elem[x][y]==' ' for elem in result)

				#smoothing 
				#k=6
				foreground=(float)(foreground_count+k)/(classLength+2*k)
				background=(float)(background_count+k)/(classLength+2*k)
				conditional_pixel_elem.append((foreground,background))
			conditional_prob_elem.append(conditional_pixel_elem)
		conditional_prob.append(conditional_prob_elem)
	return conditional_prob,priors

def chainRuleHelper(featureValue,x,y,conditional_prob,classIndex,initial):
	valueIndex=0 if featureValue=='+' or featureValue=='#' else 1
	return (initial[classIndex]+math.log(conditional_prob[classIndex][x][y][valueIndex]))

def classify(k):
	image_array,image_labels=readFile("testimages","testlabels")
	conditional_prob,priors=train(k)
	#print(conditional_prob[0][0][0])
	result=[]
	indices=range(10)
	most_prototypal_value=[float("-inf")]*10
	most_prototypal_index=[0]*10
	least_prototypal_value=[float("inf")]*10
	least_prototypal_index=[0]*10
	for j,image in enumerate(image_array):
		initial=map(math.log,priors)
		#print initial
		for x in range(28):
			for y in range(28):
				initial=map(lambda e:chainRuleHelper(image[x][y],x,y,conditional_prob,e,initial),indices)
			
		class_value=max(initial)
		class_result=initial.index(class_value)
		if class_value>most_prototypal_value[class_result]:
			most_prototypal_value[class_result]=class_value
			most_prototypal_index[class_result]=j
		if class_value<least_prototypal_value[class_result]:
			least_prototypal_value[class_result]=class_value
			least_prototypal_index[class_result]=j
		result.append(str(class_result))

	totalCount=len(image_labels)

	classCount=[0]*10
	confusion_matrix=[[0 for i in range(10)] for j in range(10)]
	for i in range(totalCount):
		experiment_class=result[i]
		#print experiment_class
		real_class=image_labels[i]
		classCount[int(real_class)]+=1
		#print real_class
		confusion_matrix[int(real_class)][int(experiment_class)]+=1
		#pp.pprint(confusion_matrix)
		#break
	for i in range(10):
		for j in range(10):
			confusion_matrix[i][j]/=float(classCount[i])
	print(confusion_matrix)
	#print(most_prototypal_value)
	#print(least_prototypal_value)

	#print"start"
	overallAccuracy=sum(image_labels[i]==result[i] for i in range(totalCount))
 	print"overall accuracy",overallAccuracy/float(totalCount)
 	#print"end"


	accuracy=[]
	for i in range(10):
		accuracy.append(confusion_matrix[i][i])
	print(accuracy)


	for i in range(10):
		print "Class: {}".format(i)
		print("most_prototypal")
		pp.pprint(image_array[most_prototypal_index[i]])

		pp.pprint("least_prototypal")
		pp.pprint(image_array[least_prototypal_index[i]])

	index_pairs=[]
	index_values=[]
	for i,e1 in enumerate(confusion_matrix):
		maximum=float("-inf")
		index=0
		for j,e2 in enumerate(e1):
			if e2>maximum and i!=j:
				maximum=e2
				index=j
		index_pairs.append((i,index))
		index_values.append(maximum)

	#print index_pairs
	#print index_values

	sorted_indices=sorted(range(10), key=lambda x: index_values[x], reverse=True)
	pairs=[index_pairs[i] for i in sorted_indices[:4]]
	print pairs
	#pairs.append((1,8))

	#odd ratios
	#pairs=[(1,8),(4,9),(5,3),(8,9),(9,3)]

	x=np.array([i for i in range(28) for j in range(28)])
	#print(x.shape)
	y=np.array([j for i in range(28) for j in range(28)])

	
	for pair in pairs:
		fig=plt.figure()
		cond_prob1=conditional_prob[pair[0]]
		image1=np.array([math.log(cond_prob1[27-j][i][0]) for i in range(28) for j in range(28)])
		#print(image1.shape)

		cond_prob2=conditional_prob[pair[1]]
		image2=np.array([math.log(cond_prob2[27-j][i][0]) for i in range(28) for j in range(28)])

		image3=np.array([math.log(cond_prob1[27-j][i][0])-math.log(cond_prob2[27-j][i][0]) for i in range(28) for j in range(28)])

		fig.add_subplot(131,aspect='equal')
		plt.scatter(x,y,c=image1)
		plt.colorbar(fraction=0.046, pad=0.04)
		plt.axis('off')

		fig.add_subplot(132,aspect='equal')
		plt.scatter(x,y,c=image2)
		plt.colorbar(fraction=0.046, pad=0.04)
		plt.axis('off')

		fig.add_subplot(133,aspect='equal')
		plt.scatter(x,y,c=image3)
		plt.axis('off')
		plt.colorbar(fraction=0.046, pad=0.04)
		plt.show()


'''
for i in range(10):
	classify(i+1)
'''
classify(0.1)
