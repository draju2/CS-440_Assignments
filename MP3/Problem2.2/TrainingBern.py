from TrainingDataBern import TrainingDataBern


class TrainingBern:

    def __init__(self):
        self.training_data = TrainingDataBern()

    def get_training_data(self):
        return self.training_data

    def train(self, filepath):
        f = open(filepath, 'r')

        lines = list(f)

        for line in lines:
            word_list = line.split()
            self.add_words(word_list)

    def add_words(self, words):

        if len(words) < 1:
            assert False
            return

        cla = int(words[0])

        self.training_data.training_data_count += 1

        if cla in self.training_data.feature_data:
            cla_list = self.training_data.feature_data[cla]
            i = cla_list[0]
            cla_list[1] += 0
            cla_list[2] += 1
        else:
            length = len(self.training_data.feature_data)
            self.training_data.feature_data[cla] = [length, 0, 1]
            i = length

        d = self.training_data.dict

        for j in range(1, len(words)):
            word_pair = words[j].split(':', 1)
            word = word_pair[0]
            count = int(word_pair[1])
            assert (count is not 0)
            if word in d:
                features = d[word]
                features[i] += 1
            else:
                d[word] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                features = d[word]
                features[i] += 1