from TrainingBern import TrainingBern
from TestBern import TestBern

train1 = TrainingBern()
#train1.train('movie_review/rt-train.txt')
#train1.train('fisher_2topic/fisher_train_2topic.txt')
train1.train('fisher_40topic/fisher_train_40topic.txt')

test1 = TestBern(train1.get_training_data())
#test1.run_test('movie_review/rt-test.txt')
#test1.run_test('fisher_2topic/fisher_test_2topic.txt')
test1.run_test('fisher_40topic/fisher_test_40topic.txt')

print(test1.actual_result_list)
print(test1.predicted_result_list)

accuracy_count = 0

for i in range(len(test1.actual_result_list)):
    if test1.actual_result_list[i] is test1.predicted_result_list[i]:
        accuracy_count += 1

print(accuracy_count / len(test1.actual_result_list))
