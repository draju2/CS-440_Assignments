class TrainingDataMulti:

    def __init__(self):
        self.dict = dict()  # word : [wc1 .. wc40]
        self.feature_data = dict()  # class : [index, total_words]
        self.training_data_count = 0

    def get_probability(self, word, cla):
        if cla in self.feature_data:
            cla_list = self.feature_data[cla]
            i = cla_list[0]
            c = cla_list[1]
        else:
            assert False
            return 0.0
        smoothingConstant = 0.015
        if word in self.dict:
            features = self.dict[word]
            count = features[i]
            return (count + smoothingConstant) / (c + smoothingConstant*len(self.dict))

        # instead of returning 0.0 for the missing word, return 1.0 to throw out word
        return 1.0

    def get_probability_prior(self, cla):
        if cla in self.feature_data:
            cla_list = self.feature_data[cla]
            c = cla_list[2]
            prob = c / self.training_data_count
            return prob
        else:
            assert False
            return 0.0