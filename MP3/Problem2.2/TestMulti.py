import math
import heapq


class TestMulti:

    def __init__(self, training_data):
        self.training_data = training_data
        self.actual_result_list = []
        self.predicted_result_list = []

    def run_test(self, path):
        f = open(path, 'r')

        lines = list(f)

        for line in lines:
            word_list = line.split()
            self.append_actual_result(word_list)
            self.append_predicted_result(word_list)

    def append_actual_result(self, words):
        if len(words) < 1:
            assert False
            return

        cla = int(words[0])

        self.actual_result_list.append(cla)

    def append_predicted_result(self, words):
        prob_heap = []

        for key in self.training_data.feature_data:
            prob = self.predict(words, key)
            heapq.heappush(prob_heap, (-1.0 * prob, key))

        (p, cla) = heapq.heappop(prob_heap)
        self.predicted_result_list.append(cla)

    def predict(self, words, cla):
        prob = 0.0

        if len(words) < 1:
            assert False
            return prob

        for i in range(1, len(words)):
            word_pair = words[i].split(':', 1)
            word = word_pair[0]
            count = int(word_pair[1])
            assert (count is not 0)
            temp_prob = math.log(self.training_data.get_probability(word, cla))
            for j in range(count):
                prob += temp_prob

        prob += math.log(self.training_data.get_probability_prior(cla))
        return prob
