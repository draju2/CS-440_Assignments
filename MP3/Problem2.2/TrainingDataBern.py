import math


class TrainingDataBern:

    def __init__(self):
        self.dict = dict()
        self.feature_data = dict()
        self.training_data_count = 0

    def get_total_probability_bern(self, words, cla):
        prob = 0

        temp_set = set()

        for i in range(1, len(words)):
            word_pair = words[i].split(':', 1)
            temp_word = word_pair[0]
            temp_set.add(temp_word)

        for word in self.dict:
            if word in temp_set:
                prob += math.log(self.get_probability_bern(word, cla))
            else:
                prob += math.log(1 - self.get_probability_bern(word, cla))

        return prob

    def get_probability_bern(self, word, cla):
        if cla in self.feature_data:
            cla_list = self.feature_data[cla]
            i = cla_list[0]
            dc = cla_list[2]
        else:
            assert False
            return 0.0

        if word in self.dict:
            features = self.dict[word]
            wc = features[i]
            return (wc + 1) / (dc + len(self.dict))
        else:
            return 1 / (dc + len(self.dict))

    # def get_probability(self, word, cla):
    #     if cla in self.feature_data:
    #         cla_list = self.feature_data[cla]
    #         i = cla_list[0]
    #         c = cla_list[1]
    #     else:
    #         assert False
    #         return 0.0
    #
    #     if word in self.dict:
    #         features = self.dict[word]
    #         count = features[i]
    #         return (count + 1) / (c + len(self.dict))
    #
    #     # instead of returning 0.0 for the missing word, return 1.0 to throw out word
    #     return 1.0

    def get_probability_prior(self, cla):
        if cla in self.feature_data:
            cla_list = self.feature_data[cla]
            c = cla_list[2]
            prob = c / self.training_data_count
            return prob
        else:
            assert False
            return 0.0