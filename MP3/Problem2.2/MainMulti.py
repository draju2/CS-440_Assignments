from TrainingMulti import TrainingMulti
from TestMulti import TestMulti

train1 = TrainingMulti()
train1.train('fisher_40topic/fisher_train_40topic.txt')

test1 = TestMulti(train1.get_training_data())
test1.run_test('fisher_40topic/fisher_test_40topic.txt')

print(test1.actual_result_list)
print(test1.predicted_result_list)

accuracy_count = 0

for i in range(len(test1.actual_result_list)):
    if test1.actual_result_list[i] is test1.predicted_result_list[i]:
        accuracy_count += 1

print(accuracy_count / len(test1.actual_result_list))

# train2 = TrainingMulti()
# train2.train('fisher_2topic/fisher_train_2topic.txt')
#
# test2 = TestMulti(train2.get_training_data())
# test2.run_test('fisher_2topic/fisher_test_2topic.txt')
#
# print(test2.actual_result_list)
# print(test2.predicted_result_list)
#
# accuracy_count = 0
# inaccuracy_count = 0
#
# for i in range(len(test2.actual_result_list)):
#     if test2.actual_result_list[i] is test2.predicted_result_list[i]:
#         accuracy_count += 1
#     else:
#         inaccuracy_count += 1
#
# print(accuracy_count / len(test2.actual_result_list))
# print(inaccuracy_count / len(test2.actual_result_list))
# print(len(test2.actual_result_list))
# print(len(train2.get_training_data().dict))


