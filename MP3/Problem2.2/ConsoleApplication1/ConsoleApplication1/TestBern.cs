﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication5
{
    class TestBern
    {
        public TrainingDataBern m_TrainingData;
        public List<int> m_ActualResults = new List<int>();
        public List<int> m_PredictedResults = new List<int>();

        public TestBern(TrainingDataBern tdb)
        {
            m_TrainingData = tdb;
        }

        public void RunTest(string sPath)
        {
            string[] lines = File.ReadAllLines(sPath);

            foreach (string line in lines)
            {
                string[] words = line.Split();
                AppendActualResult(words);
                AppendPredictedResults(words);
            }
        }

        private void AppendActualResult(string[] words)
        {
            if (words.Length < 1)
            {
                Debug.Assert(false);
                return;
            }

            int cla = int.Parse(words[0]);

            m_ActualResults.Add(cla);
        }

        private void AppendPredictedResults(string[] words)
        {
            SortedSet<Tuple<double, int>> sd = new SortedSet<Tuple<double, int>>();

            foreach (KeyValuePair<int, FeatureData> pair in m_TrainingData.m_FeatureDict)
            {
                double prob = PredictBern(words, pair.Key);
                sd.Add(Tuple.Create<double, int>(prob, pair.Key));
            }

            int cla = sd.Last().Item2;
            m_PredictedResults.Add(cla);
        }

        private double PredictBern(string[] words, int cla)
        {
            if (words.Length < 1)
            {
                Debug.Assert(false);
                return 0.0;
            }

            double prob = m_TrainingData.GetTotalProbability(words, cla);

            prob += Math.Log(m_TrainingData.GetProbabilityPrior(cla));

            return prob;
        }
    }
}


