﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication5
{
    class Program
    {
        static void Main(string[] args)
        {
            string sDir = @"C:\Users\Neil\Documents\Visual Studio 2015\Projects\CS-440_Assignments\MP3\Problem2.2\ConsoleApplication1\ConsoleApplication1";

            TrainingBern train1 = new TrainingBern();
            //trdain1.Train(sDir + "\\movie_review\\rt-train.txt");
            //train1.Train(sDir + "\\fisher_2topic\\fisher_train_2topic.txt");
            train1.Train(sDir + "\\fisher_40topic\\fisher_train_40topic.txt");

            TestBern test1 = new TestBern(train1.m_TrainingData);
            //tdest1.RunTest(sDir + "\\movie_review\\rt-test.txt");
            //test1.RunTest(sDir + "\\fisher_2topic\\fisher_test_2topic.txt");
            test1.RunTest(sDir + "\\fisher_40topic\\fisher_test_40topic.txt");

            int accuracyCount = 0;

            int[,] confusionMatrix = new int[40, 40];

            for(int i = 0; i < test1.m_ActualResults.Count; i++)
            {
                confusionMatrix[test1.m_ActualResults[i], test1.m_PredictedResults[i]] += 1;

                if (test1.m_ActualResults[i] == test1.m_PredictedResults[i])
                    accuracyCount++;
            }

            double acc = (double)accuracyCount / (double)test1.m_ActualResults.Count;
            Console.WriteLine(acc);

            StreamWriter stream = new StreamWriter(File.Create(sDir + "\\ConfusionMatrixBern.txt"));

            for (int i = 0; i < 40; i++)
            {
                for (int j = 0; j < 40; j++)
                {
                    Console.Write(confusionMatrix[i, j] + ",");
                    stream.Write(confusionMatrix[i, j] + ",");
                }
                Console.WriteLine();
                stream.WriteLine();
            }

            Console.ReadKey();
        }
    }
}
