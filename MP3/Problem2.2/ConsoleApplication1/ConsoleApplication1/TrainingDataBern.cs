﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication5
{
    class TrainingDataBern
    {
        public Dictionary<string, int[]> m_Dict = new Dictionary<string, int[]>();
        public Dictionary<int, FeatureData> m_FeatureDict = new Dictionary<int, FeatureData>();
        public int m_TrainingDataCount = 0;

        public double GetTotalProbability(string[] words, int cla)
        {
            double prob = 0.0;

            HashSet<string> tempSet = new HashSet<string>();

            for(int i = 1; i < words.Length; i++)
            {
                string[] wordpair = words[i].Split(':');
                string tempword = wordpair[0];
                tempSet.Add(tempword);
            }

            foreach(KeyValuePair<string, int[]> pair in m_Dict)
            {
                if (tempSet.Contains(pair.Key))
                {
                    double p = GetProbability(pair.Key, cla);
                    prob += Math.Log(p);
                }
                else
                {
                    double p = GetProbability(pair.Key, cla);
                    prob += Math.Log(1 - p);
                }
            }

            return prob;

        }

        public double GetProbability(string word, int cla)
        {
            int i = -1;
            double dc = 0.0;

            if (m_FeatureDict.ContainsKey(cla))
            {
                FeatureData fd = m_FeatureDict[cla];
                i = fd.index;
                dc = (double)fd.docCount;
            }
            else
            {
                Debug.Assert(false);
                return 0.0;
            }

            double smoothingConstant = 0.001;

            if (m_Dict.ContainsKey(word))
            {
                int[] features = m_Dict[word];
                double wc = (double)features[i];
                return (wc + smoothingConstant) / (dc + smoothingConstant * (double)m_Dict.Count);
            }
            else
            {
                return 1 / (dc + smoothingConstant * (double)m_Dict.Count);
            }
        }

        public double GetProbabilityPrior(int cla)
        {
            if (m_FeatureDict.ContainsKey(cla))
            {
                FeatureData fd = m_FeatureDict[cla];
                double dc = fd.docCount;
                return dc / (double)m_TrainingDataCount;
            }
            else
            {
                Debug.Assert(false);
                return 0.0;
            }
        }
    }

    public class FeatureData
    {
        public int index = 0;
        public int docCount = 0;
    }
}
