﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication5
{
    class TrainingBern
    {
        public TrainingDataBern m_TrainingData = new TrainingDataBern();

        public void Train(string sPath)
        {
            string[] lines = File.ReadAllLines(sPath);

            foreach (string line in lines)
            {
                string[] words = line.Split();
                AddWords(words);
            }
        }

        private void AddWords(string[] words)
        {
            if (words.Length < 1)
            {
                Debug.Assert(false);
                return;
            }

            int cla = int.Parse(words[0]);

            m_TrainingData.m_TrainingDataCount++;

            int index;

            if (m_TrainingData.m_FeatureDict.ContainsKey(cla))
            {
                FeatureData fd = m_TrainingData.m_FeatureDict[cla];
                fd.docCount++;
                index = fd.index;
            }
            else
            {
                index = m_TrainingData.m_FeatureDict.Count;
                FeatureData fd = new FeatureData();
                fd.docCount = 1;
                fd.index = index;
                m_TrainingData.m_FeatureDict.Add(cla, fd);
            }

            for(int j = 1; j < words.Length; j++)
            {
                string[] wordpair = words[j].Split(':');
                string word = wordpair[0];

                if (m_TrainingData.m_Dict.ContainsKey(word))
                {
                    int[] features = m_TrainingData.m_Dict[word];
                    features[index] += 1;
                }
                else
                {
                    m_TrainingData.m_Dict.Add(word, new int[40]);
                    int[] features = m_TrainingData.m_Dict[word];
                    features[index] += 1;
                }
            }
        }
    }
}
