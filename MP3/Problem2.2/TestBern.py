import heapq
import math


class TestBern:

    def __init__(self, training_data):
        self.training_data = training_data
        self.actual_result_list = []
        self.predicted_result_list = []

    def run_test(self, path):
        f = open(path, 'r')

        lines = list(f)

        for line in lines:
            word_list = line.split()
            self.append_actual_result(word_list)
            self.append_predicted_result_bern(word_list)

    def append_actual_result(self, words):
        if len(words) < 1:
            assert False
            return

        cla = int(words[0])

        self.actual_result_list.append(cla)

    def append_predicted_result_bern(self, words):
        prob_heap = []

        for key in self.training_data.feature_data:
            prob = self.predict_bern(words, key)
            heapq.heappush(prob_heap, (-1.0 * prob, key))

        (p, cla) = heapq.heappop(prob_heap)
        self.predicted_result_list.append(cla)

    def predict_bern(self, words, cla):

        if len(words) < 1:
            assert False
            return 0

        prob = self.training_data.get_total_probability_bern(words, cla)

        prob += math.log(self.training_data.get_probability_prior(cla))

        return prob
