class TrainingDataMulti:

    dict = None
    feature_data = None

    def __init__(self):
        self.dict = dict()
        self.feature_data = dict()

    def get_probability(self, word, cla):
        if cla in self.feature_data:
            cla_list = self.feature_data[cla]
            i = cla_list[0]
            c = cla_list[1]
        else:
            assert False
            return 0.0

        if word in self.dict:
            features = self.dict[word]
            count = features[i]
            return (count + 1) / (c + len(self.dict))

        # instead of returning 0.0 for the missing word, return 1.0 to throw out word
        return 1.0
