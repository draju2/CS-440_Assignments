from Training import Training
from Test import Test

train1 = Training()
train1.train('fisher_2topic/fisher_train_2topic.txt')

test1 = Test(train1.get_training_data())
test1.run_test('fisher_2topic/fisher_test_2topic.txt')

print(test1.actual_result_list)
print(test1.predicted_result_list)
print(test1.predicted_result_list_bern)

accuracy_count = 0
accuracy_count_bern = 0

for i in range(len(test1.actual_result_list)):
    if test1.actual_result_list[i] is test1.predicted_result_list[i]:
        accuracy_count += 1

for i in range(len(test1.actual_result_list)):
    if test1.actual_result_list[i] is test1.predicted_result_list_bern[i]:
        accuracy_count_bern += 1

print(accuracy_count / len(test1.actual_result_list))
print(accuracy_count_bern / len(test1.actual_result_list))

#training_data = train1.training_data

#print(training_data.dict_pos)
#print(training_data.get_probability('hanks', 1))
#print(training_data.get_probability('old-fashioned', 1))
