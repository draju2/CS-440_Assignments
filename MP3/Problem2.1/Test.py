import math


class Test(object):

    training_data = None
    actual_result_list = []
    predicted_result_list = []
    predicted_result_list_bern = []

    def __init__(self, training_data):
        self.training_data = training_data

    def run_test(self, path):
        f = open(path, 'r')

        lines = list(f)

        for line in lines:
            word_list = line.split()
            self.append_actual_result(word_list)
            self.append_predicted_result(word_list)
            self.append_predicted_result_bern(word_list)

    def append_actual_result(self, words):
        if len(words) < 1:
            assert False
            return

        cla = int(words[0])

        self.actual_result_list.append(cla)

    def append_predicted_result(self, words):
        prob_pos = self.predict(words, 1)
        prob_neg = self.predict(words, -1)

        if prob_neg > prob_pos:
            self.predicted_result_list.append(-1)
        else:
            self.predicted_result_list.append(1)

    def append_predicted_result_bern(self, words):
        prob_pos = self.predict_bern(words, 1)
        prob_neg = self.predict_bern(words, -1)

        if prob_neg > prob_pos:
            self.predicted_result_list_bern.append(-1)
        else:
            self.predicted_result_list_bern.append(1)

    def predict(self, words, cla):
        prob = 0

        if len(words) < 1:
            assert False
            return prob

        for i in range(1, len(words)):
            word_pair = words[i].split(':', 1)
            word = word_pair[0]
            count = int(word_pair[1])
            assert (count is not 0)
            for j in range(count):
                prob += math.log(self.training_data.get_probability(word, cla))

        return prob

    def predict_bern(self, words, cla):

        if len(words) < 1:
            assert False
            return 0

        prob = self.training_data.get_total_probability_bern(words, cla)

        return prob
