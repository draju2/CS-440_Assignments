from TrainingData import TrainingData

class Training(object):

    training_data = TrainingData()

    def get_training_data(self):
        return self.training_data

    def train(self, filepath):
        f = open(filepath, 'r')

        lines = list(f)

        for line in lines:
            word_list = line.split()
            self.add_words(word_list)
            self.add_words_bern(word_list)

    def add_words(self, words):

        if len(words) < 1:
            assert False
            return

        cla = int(words[0])

        word_count = len(words) - 1

        if cla is 1:
            self.training_data.count_pos += word_count
            d = self.training_data.dict_pos
        elif cla is -1:
            self.training_data.count_neg += word_count
            d = self.training_data.dict_neg
        else:
            assert False
            return

        for i in range(1, len(words)):
            word_pair = words[i].split(':', 1)
            word = word_pair[0]
            count = int(word_pair[1])
            assert (count is not 0)
            if word in d:
                d[word] += count
            else:
                d[word] = count

    def add_words_bern(self, words):
        if len(words) < 1:
            assert False
            return

        cla = int(words[0])

        if cla is 1:
            self.training_data.count_doc_pos += 1
            d = self.training_data.dict_pos_bern
        elif cla is -1:
            self.training_data.count_doc_neg += 1
            d = self.training_data.dict_neg_bern
        else:
            assert False
            return

        for i in range(1, len(words)):
            word_pair = words[i].split(':', 1)
            word = word_pair[0]
            count = int(word_pair[1])
            assert (count is not 0)
            if word in d:
                d[word] += 1
            else:
                d[word] = 1
