import math


class TrainingData(object):

    dict_pos = dict()
    dict_neg = dict()
    dict_pos_bern = dict()
    dict_neg_bern = dict()
    count_pos = 0
    count_neg = 0
    count_doc_pos = 0
    count_doc_neg = 0

    def get_probability(self, word, cla):
        if cla is 1:
            d = self.dict_pos
            c = self.count_pos
        elif cla is -1:
            d = self.dict_neg
            c = self.count_neg
        else:
            assert False
            return 0.0

        if word in d:
            return (d[word] + 1) / (c + len(d))
        else:
            return 1 / (c + len(d))

    def get_total_probability_bern(self, words, cla):
        prob = 0

        if cla is 1:
            d = self.dict_pos_bern
            c = self.count_doc_pos
        elif cla is -1:
            d = self.dict_neg_bern
            c = self.count_doc_neg
        else:
            assert False
            return 0.0

        temp_set = set()

        for i in range(1, len(words)):
            word_pair = words[i].split(':', 1)
            temp_word = word_pair[0]
            temp_set.add(temp_word)

        for word in d:
            if word in temp_set:
                prob += math.log(self.get_probability_bern(word, cla))
            else:
                prob += math.log(1 - self.get_probability_bern(word, cla))

        return prob

    def get_probability_bern(self, word, cla):
        if cla is 1:
            d = self.dict_pos_bern
            c = self.count_doc_pos
        elif cla is -1:
            d = self.dict_neg_bern
            c = self.count_doc_neg
        else:
            assert False
            return 0.0

        if word in d:
            return (d[word] + 1) / (c + len(d))
        else:
            return 1 / (c + len(d))
