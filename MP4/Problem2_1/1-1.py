import pprint as pp
import math
import numpy as np
import matplotlib.pyplot as plt
import random

'''
Ordering of training examples (fixed vs. random);
'''

bias=True
randomOrder=True

def readFile(imageFileName,labelFileName):
	fd_images = open(imageFileName,"r")
	fd_labels = open(labelFileName,"r")

	image_array = []
	image_elem = []
	count = 0
	for i,line in enumerate(fd_images.readlines()):
		if i != 0 and i%28 == 0:
			if bias:
				image_elem.append(1)
			image_array.append(np.array(image_elem))
			image_elem = []
			count = i
		temp=[0 if elem==' ' else 1 for elem in line[:-1]]
		image_elem.extend(temp)

	if bias:
		image_elem.append(1)
	image_array.append(np.array(image_elem))
	
	image_labels = [line.strip() for line in fd_labels.readlines()]

	return image_array,image_labels

def train(imageFileName,labelFileName):
	#read training set and data set
	trainimage_array,trainimage_labels=readFile(imageFileName,labelFileName)

	#initialize 
	weight_matrix=np.zeros((10, 785)) if bias else np.zeros((10, 784))	#Initialization of weights (zeros vs. random);
	if bias:
		for i in range(10):
			weight_matrix[i][784]=0
	epoch=20	#Number of epochs.
	
	if randomOrder:
		order=range(len(trainimage_array))

	#train
	for i in range(epoch):
		alpha=1000/(1000.0+i)#Learning rate decay function;

		if randomOrder:
			random.shuffle(order)
			trainimage_array=[trainimage_array[i] for i in order]
			trainimage_labels=[trainimage_labels[i] for i in order]
		#print trainimage_array[0]
		
		for j,train_image in enumerate(trainimage_array):
			result=np.dot(weight_matrix,train_image)
			result_class=np.argmax(result)
			
			actual_class=trainimage_labels[j]

			#update weights in cases of misclassifying
			if str(result_class)!=actual_class:
				difference=alpha*train_image
				weight_matrix[int(actual_class)]+=difference
				weight_matrix[result_class]-=difference

	return weight_matrix

def classify():
	testimage_array,testimage_labels=readFile("digitdata/testimages","digitdata/testlabels")
	weight_matrix=train("digitdata/trainingimages","digitdata/traininglabels")

	#classify
	test_result=[]
	for j,train_image in enumerate(testimage_array):
		result=np.dot(weight_matrix,train_image)
		result_class=np.argmax(result)
		test_result.append(str(result_class))

	
	totalCount=len(testimage_labels)
	overallAccuracy=sum(testimage_labels[i]==test_result[i] for i in range(totalCount))
 	print"overall accuracy",overallAccuracy/float(totalCount)
	
#train("digitdata/trainingimages","digitdata/traininglabels")
classify()