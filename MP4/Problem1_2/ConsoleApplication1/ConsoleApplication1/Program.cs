﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{

	class Program
	{
		static double[] paddle_map = new double[12];
		static double[] presence_map = new double[12];
		static double[,] ball_map = new double[12, 12];
		static void Main(string[] args)
		{
			// runtest1();
			playgames();
		}

		static public void runtest1()
		{
			game Game = new game(0.5, 0.5, 0.03, 0.01, 0.4, 0.2);
			Random rand_ = new Random();
			while (Game.execute_move(rand_.Next(0,3)) != -1)
			{
				Console.Write(Game.ball_x);
				Console.Write('\t');
				Console.Write(Game.ball_y);
				Console.Write('\t');
				Console.Write(Game.paddle_y);
				Console.Write('\n');
			}
		}

		static public int playGame(double[,,,,,] Qvals, double[,,,,,] N, int m, int n, int vx, int vy, int py)
		{
			game Game = new game(0.5, 0.5, 0.03, 0.01, 0.4, 0.2);
			int t_decay = 500;
			int t = 0;
			double alpha = 1.0;
			double gamma = 0.99;
			int action = getMove(Qvals, N, Game, m, n, vx, vy, py);
			int score = 0;
			while (Game.isgameover() == false)
			{
				var disc_state = Game.get_disc_state(n, m, vx, vy, py);
				int x = disc_state.Item1;
				int y = disc_state.Item2;
				int v_x = disc_state.Item3;
				int v_y = disc_state.Item4;
				int p_y = disc_state.Item5;
				presence_map[p_y] += 1;
				ball_map[x, y] += 1;
				alpha = (double)t_decay / (double)(t_decay + N[x, y, (v_x + 1) / 2, v_y + 1, p_y, action + 1]);
				int reward = Game.execute_move(action);
				if (reward == 1)
				{
					score++;
					paddle_map[p_y] += 1;
				}
				if(score>10)
				{
					;
				}
				double payoff_est = 0;
				if(!Game.isgameover())
				{
					disc_state = Game.get_disc_state(n, m, vx, vy, py);
					int xn = disc_state.Item1;
					int yn = disc_state.Item2;
					int v_xn = disc_state.Item3;
					int v_yn = disc_state.Item4;
					int p_yn = disc_state.Item5;
					if (xn > 11 || xn < 0)
					{
						Console.WriteLine("xn");
						Console.WriteLine(Game.ball_x);
						Console.WriteLine(Game.velocity_x);
						Console.WriteLine(score);
						Console.WriteLine(xn);
					}
					if (yn > 11 || yn < 0)
					{
						Console.WriteLine("yn");
						Console.WriteLine(Game.ball_y);
						Console.WriteLine(yn);
					}

					if((v_xn+1)/2<0 || (v_xn + 1) / 2 > 1)
					{
						Console.WriteLine("vx");
						Console.WriteLine(v_xn);
					}

					if ((v_yn + 1)  < 0 || (v_yn + 1) > 2)
					{
						Console.WriteLine("vy");
						Console.WriteLine(v_yn);
					}
					if (p_yn > 12 || p_yn < 0)
					{
						Console.WriteLine("py");
						Console.WriteLine(p_yn);
					}
					payoff_est = Qvals[xn, yn, (v_xn + 1) / 2, v_yn + 1, p_yn, 0];
					for (int i = 1; i < 3; i++)
					{
						if (Qvals[xn, yn, (v_xn + 1) / 2, v_yn + 1, p_yn, i] >= payoff_est)
							payoff_est = Qvals[xn, yn, (v_xn + 1) / 2, v_yn + 1, p_yn, i];
					}

				}

				Qvals[x, y, (v_x+1)/2, v_y+1, p_y, action + 1] += alpha * (reward + gamma * payoff_est - Qvals[x, y, (v_x+1)/2, v_y+1, p_y, action + 1]);
				N[x, y, (v_x+1)/2, v_y+1, p_y, action + 1] += 1;
				action = getMove(Qvals, N, Game, m, n, vx, vy, py);
				t++;
			}
			Console.Write(t);
			Console.Write('\t');
			Console.Write(score);
			Console.Write('\n');
			return score;
		}

		static int getMove(double[,,,,,] Qvals, double[,,,,,] N,game Game, int m, int n, int vx, int vy, int py)
		{
			if (!Game.isgameover())
			{
				int ne = 20;
				double k = 30;
				var disc_state = Game.get_disc_state(n, m, vx, vy, py);
				int x = disc_state.Item1;
				int y = disc_state.Item2;
				int v_x = disc_state.Item3;
				int v_y = disc_state.Item4;
				int p_y = disc_state.Item5;
				int action = -1;
				double payoff_est = Qvals[x, y, (v_x + 1) / 2, v_y + 1, p_y, 0];
				if (N[x, y, (v_x+1)/2, v_y+1, p_y, 0] < ne)
					payoff_est = Qvals[x, y, (v_x + 1) / 2, v_y + 1, p_y, 0] + k / N[x, y, (v_x + 1) / 2, v_y + 1, p_y, 0];
				for (int i = 1; i < 3; i++)
				{
					double temp_payoff_est = 0;
					if (N[x, y, (v_x + 1) / 2, v_y + 1, p_y, i] < ne)
						temp_payoff_est = Qvals[x, y, (v_x + 1) / 2, v_y + 1, p_y, i] + k / N[x, y, (v_x + 1) / 2, v_y + 1, p_y, i];
					else
						temp_payoff_est = Qvals[x, y, (v_x + 1) / 2, v_y + 1, p_y, i];
					if (temp_payoff_est >= payoff_est)
					{
						payoff_est = temp_payoff_est;
						action = i-1;
					}
				}
				return action;
			}
			else
				return 0;

		}


		static public int PlayGameWithOpponent(double[,,,,,] Qvals, double[,,,,,] N, int m, int n, int vx, int vy, int py)
		{
			GameTwoPlayer Game = new GameTwoPlayer(0.5, 0.5, 0.03, 0.01, 0.4, 0.2, 0.4);
			//int t_decay = 500;
			//int t = 0;
			int action = getMoveTwoPlayer(Qvals, N, Game, m, n, vx, vy, py);
			//int score = 0;
			while (Game.isgameover() == false)
			{
				var disc_state = Game.get_disc_state(n, m, vx, vy, py);
				int x = disc_state.Item1;
				int y = disc_state.Item2;
				int v_x = disc_state.Item3;
				int v_y = disc_state.Item4;
				int p_y = disc_state.Item5;
				presence_map[p_y] += 1;
				ball_map[x, y] += 1;
				//alpha = (double)t_decay / (double)(t_decay + N[x, y, (v_x + 1) / 2, v_y + 1, p_y, action + 1]);
				Game.execute_move(action);
				//if (reward == 1)
				//{
				//	score++;
				//	paddle_map[p_y] += 1;
				//}
				//if(score>10)
				//{
				//	;
				//}
				double payoff_est = 0;
				if(!Game.isgameover())
				{
					disc_state = Game.get_disc_state(n, m, vx, vy, py);
					int xn = disc_state.Item1;
					int yn = disc_state.Item2;
					int v_xn = disc_state.Item3;
					int v_yn = disc_state.Item4;
					int p_yn = disc_state.Item5;

					payoff_est = Qvals[xn, yn, (v_xn + 1) / 2, v_yn + 1, p_yn, 0];
					for (int i = 1; i < 3; i++)
					{
						if (Qvals[xn, yn, (v_xn + 1) / 2, v_yn + 1, p_yn, i] >= payoff_est)
							payoff_est = Qvals[xn, yn, (v_xn + 1) / 2, v_yn + 1, p_yn, i];
					}

				}

				//Qvals[x, y, (v_x + 1) / 2, v_y + 1, p_y, action + 1] += alpha * (reward + gamma * payoff_est - Qvals[x, y, (v_x + 1) / 2, v_y + 1, p_y, action + 1]);
				//N[x, y, (v_x + 1) / 2, v_y + 1, p_y, action + 1] += 1;
				action = getMoveTwoPlayer(Qvals, N, Game, m, n, vx, vy, py);
				//t++;
			}

			return Game.GameWinner();
		}

		static int getMoveTwoPlayer(double[,,,,,] Qvals, double[,,,,,] N,GameTwoPlayer Game, int m, int n, int vx, int vy, int py)
		{
			if (!Game.isgameover())
			{
				var disc_state = Game.get_disc_state(n, m, vx, vy, py);
				int x = disc_state.Item1;
				int y = disc_state.Item2;
				int v_x = disc_state.Item3;
				int v_y = disc_state.Item4;
				int p_y = disc_state.Item5;
				int action = -1;
				double payoff_est = Qvals[x, y, (v_x + 1) / 2, v_y + 1, p_y, 0];
//				if (N[x, y, (v_x+1)/2, v_y+1, p_y, 0] < ne)
//					payoff_est = Qvals[x, y, (v_x + 1) / 2, v_y + 1, p_y, 0] + k / N[x, y, (v_x + 1) / 2, v_y + 1, p_y, 0];
				for (int i = 1; i < 3; i++)
				{
//					double temp_payoff_est = 0;
//					if (N[x, y, (v_x + 1) / 2, v_y + 1, p_y, i] < ne)
//						temp_payoff_est = Qvals[x, y, (v_x + 1) / 2, v_y + 1, p_y, i] + k / N[x, y, (v_x + 1) / 2, v_y + 1, p_y, i];
//					else
					double temp_payoff_est = Qvals[x, y, (v_x + 1) / 2, v_y + 1, p_y, i];
					if (temp_payoff_est >= payoff_est)
					{
						payoff_est = temp_payoff_est;
						action = i-1;
					}
				}
				return action;
			}
			else
				return 0;

		}

		static void playgames()
		{
			int m = 12;
			int n = 12;
			int vx = 2;
			int vy = 3;
			int py = 12;
			double[,,,,,] Qvals = new double[n, m, vx, vy, py, 3];
			double[,,,,,] N = new double[n, m, vx, vy, py, 3];
			int n_games = 0;
			while (n_games<100000)
			{
				if(n_games == 80000)
				{
					;
				}
				playGame(Qvals, N, m, n, vx, vy, py);
				Console.WriteLine(n_games);
				n_games++;
			}

			int wins = 0;
			for (int i = 0; i < 1000; i++)
			{
				int iWinner = PlayGameWithOpponent(Qvals, N, m, n, vx, vy, py);

				if (iWinner > 0)
					wins++;
			}

			Console.WriteLine(wins + "of 1000");

//			n_games = 0;
//			int[] scores = new int[2000];
//			double sum = 0;
//			while (n_games<2000)
//			{
//				scores[n_games] = playGame(Qvals, N, m, n, vx, vy, py);
//				sum += scores[n_games];
//				n_games++;
//			}
//			sum = sum / (n_games - 1);
//			double var = 0;
//			for (int i = 0; i<n_games-1; i++)
//			{
//				var += (scores[i] - sum) * (scores[i] - sum);
//			}
//			var /= (n_games - 2);
//			Console.WriteLine(sum);
//			Console.WriteLine(var);
//			double paddle_map_sum = 0;
//			double presence_map_sum = 0;
//			for (int i = 0; i < 12; i++)
//			{
//				paddle_map_sum += paddle_map[i];
//				presence_map_sum += presence_map[i];
//			}
//
//			for (int i = 0; i < 12; i++)
//			{
//				paddle_map[i] /= paddle_map_sum;
//				Console.WriteLine(paddle_map[i]);
//			}
//			for (int i = 0; i < 12; i++)
//			{
//				presence_map[i] /= presence_map_sum;
//				Console.WriteLine(presence_map[i]);
//			}
//			double ball_map_sum = 0;
//			for (int i = 0; i < 12; i++)
//			{
//				for (int j = 0; j < 12; j++)
//				{
//					ball_map_sum += ball_map[i, j];
//				}
//			}
//			for (int i = 0; i < 12; i++)
//			{
//				for (int j = 0; j < 12; j++)
//				{
//					ball_map[i, j] /= ball_map_sum;
//					ball_map[i, j] *= 100;
//					Console.Write(Math.Round(ball_map[i, j], 2));
//					Console.Write("   ");
//				}
//				Console.Write('\n');
//			}

		}
	}
}
