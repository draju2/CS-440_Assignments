﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class game
    {
        public double ball_x { get; private set; }
        public double ball_y { get; private set; }
        public double velocity_x { get; private set; }
        public double velocity_y { get; private set; }
        public double paddle_y { get; private set; }
        public double paddle_height { get; private set; }

        public game(double ball_x, double ball_y, double velocity_x, double velocity_y, double paddle_y, double paddle_height)
        {
            this.ball_x = ball_x;
            this.ball_y = ball_y;
            this.velocity_x = velocity_x;
            this.velocity_y = velocity_y;
            this.paddle_y = paddle_y;
            this.paddle_height = paddle_height;
        }

        public bool isgameover()
        {
            if (ball_x > 1.0)
                return true;
            else
                return false;
        }

        public int reward (bool is_bounce)
        {
            if (is_bounce)
                return 1;
            else if (isgameover())
                return -1;
            else
                return 0;
        }

        // Function to check which corners to do first
        public int corner_checker()
        {
            double new_y_pos = this.ball_y + this.velocity_y;
            double new_x_pos = this.ball_x + this.velocity_x;
            bool outx = !(new_x_pos > 0 && new_x_pos < 1);
            bool outy = !(new_y_pos > 0 && new_y_pos < 1);

            //Either way is fine
            if (!outx && !outy)
                return 0;
            // bounces in x first
            else if (outx && !outy)
                return 1;
            //bounces in y first
            else if (!outx && outy)
                return -1;
            else
            {
                double y_intercept = 0;
                if(new_x_pos>1)
                {
                    y_intercept = (1 - this.ball_x) * (this.velocity_y / this.velocity_x) + this.ball_y;
                    //bounces in x first
                    if (y_intercept < 1 && y_intercept > 0)
                        return 1;
                    else
                        return -1;
                }
                else
                {
                    y_intercept = -this.ball_x * (this.velocity_y / this.velocity_x) + this.ball_y;
                    //bounces in x first
                    if (y_intercept < 1 && y_intercept > 0)
                        return 1;
                    else
                        return -1;
                }
            }
        }

        public bool move_ball()
        {
            bool is_bounce = false;
            int cornering = corner_checker();

            this.ball_x = this.ball_x + this.velocity_x;
            this.ball_y = this.ball_y + this.velocity_y;

            if (cornering == -1)
            {
                if (this.ball_y >= 1.0)
                    top_bounce();
                else if (this.ball_y <= 0.0)
                    bottom_bounce();

                if (this.ball_x <= 0.0)
                    back_bounce();
                else if (this.ball_x >= 1.0)
                {
                    is_bounce = paddle_bounce();
                }
            }

            else
            {
                if (this.ball_x <= 0.0)
                    back_bounce();
                else if (this.ball_x >= 1.0)
                {
                    is_bounce = paddle_bounce();
                }

                if (this.ball_y >= 1.0)
                    top_bounce();
                else if (this.ball_y <= 0.0)
                    bottom_bounce();
            }


            return is_bounce;
        }
        public void top_bounce()
        {
            this.ball_y = 2 - this.ball_y;
            this.velocity_y = -this.velocity_y;
        }
        public void bottom_bounce()
        {
            this.ball_y = -this.ball_y;
            this.velocity_y = -this.velocity_y;
        }
        public void back_bounce()
        {
            this.ball_x = -this.ball_x;
            this.velocity_x = -this.velocity_x;
        }
        public bool paddle_bounce()
        {
            double y_intercept = this.ball_y - ((this.ball_x - 1) * this.velocity_y / this.velocity_x);
            if (!(y_intercept > 0 && y_intercept < 1))
                Console.WriteLine("Major breakdown");

            if (y_intercept > this.paddle_y && y_intercept < this.paddle_y + 0.2)
            {
                Random rand = new Random();
                double U = rand.NextDouble() * 0.03 - 0.015;
                double V = rand.NextDouble() * 0.06 - 0.03;
                this.ball_x = 2 - this.ball_x;
                this.velocity_x = -this.velocity_x + U;
                if (Math.Abs(this.velocity_x) < 0.03)
                    this.velocity_x = Math.Sign(this.velocity_x) * 0.03;
                if (Math.Abs(this.velocity_x) > 0.9)
                    this.velocity_x = Math.Sign(this.velocity_x) * 0.9;
                this.velocity_y = this.velocity_y + V;
                return true;
            }
            else
                return false;
        }
        // 1 is up -1 is down 0 is nothing
        public void move_paddle(int action)
        {
            if (action == 1)
            {
                this.paddle_y = this.paddle_y + 0.04;
                if (this.paddle_y > 1-this.paddle_height)
                    this.paddle_y = 1-this.paddle_height;
            }
            else if (action == -1)
            {
                this.paddle_y = this.paddle_y - 0.04;
                if (this.paddle_y < 0)
                    this.paddle_y = 0;
            }
            else
                this.paddle_y = this.paddle_y;
        }

        public int execute_move(int action)
        {
            bool is_bounce = false;
            move_paddle(action);
            is_bounce = move_ball();
            return reward(is_bounce);
        }

        public Tuple<int,int,int,int,int> get_disc_state(int n, int m, int vx, int vy, int py)
        {
            int disc_x = (int) Math.Floor( this.ball_x * n);
            int disc_y = (int)Math.Floor(this.ball_y * m);
            int disc_vel_x = Math.Sign(this.velocity_x);
            int disc_vel_y = 0;
            if (Math.Abs(this.velocity_y) > 0.015)
                disc_vel_y = Math.Sign(this.velocity_y);
            int disc_pad_y = (int)Math.Floor(this.paddle_y / (1 - this.paddle_height) * py);
            if (disc_pad_y == py)
                disc_pad_y = py - 1;
            if (disc_y == m)
                disc_y = m - 1;
            return Tuple.Create(disc_x, disc_y, disc_vel_x, disc_vel_y, disc_pad_y);
        }

    }
}
