import pprint as pp
import math
import numpy as np
import matplotlib.pyplot as plt
import random
from numpy import linalg as LA

def readFile(imageFileName,labelFileName):
	fd_images = open(imageFileName,"r")
	fd_labels = open(labelFileName,"r")

	image_array = []
	image_elem = []
	count = 0
	for i,line in enumerate(fd_images.readlines()):
		if i != 0 and i%28 == 0:
			image_array.append(np.array(image_elem))
			image_elem = []
			count = i
		temp=[0 if elem==' ' else 1 for elem in line[:-1]]
		image_elem.extend(temp)

	image_array.append(np.array(image_elem))
	
	image_labels = [int(line.strip()) for line in fd_labels.readlines()]

	return image_array,image_labels

def find_neighbors(trainimage_array,k,p,testimage):
	all_distances=np.array([LA.norm(trainimage-testimage,p) for trainimage in trainimage_array])
	idx = np.argpartition(all_distances, k)
	return idx[:k]

def classify(k,p):
	testimage_array,testimage_labels=readFile("digitdata/testimages","digitdata/testlabels")
	trainimage_array,trainimage_labels=readFile("digitdata/trainingimages","digitdata/traininglabels")

	test_result=[]
	for testimage in testimage_array:
		neighbors_indices=find_neighbors(trainimage_array,k,p,testimage)
		neighbors_labels=[trainimage_labels[index] for index in neighbors_indices]
		counts = np.bincount(neighbors_labels)
		result=np.argmax(counts)
		#print result
		test_result.append(result)

	totalCount=len(testimage_labels)
	overallAccuracy=sum(testimage_labels[i]==test_result[i] for i in range(totalCount))
 	print"overall accuracy",overallAccuracy/float(totalCount)

 	print "k is ",k

 	confusion_matrix=[[0 for i in range(10)] for j in range(10)]
	classCount=[0]*10
	for i in range(totalCount):
		experiment_class=test_result[i]
		real_class=testimage_labels[i]
		classCount[int(real_class)]+=1
		confusion_matrix[int(real_class)][int(experiment_class)]+=1

	for i in range(10):
		for j in range(10):
			confusion_matrix[i][j]/=float(classCount[i])
	confusion_matrix = np.array(confusion_matrix)
	#np.set_printoptions(3)
	print(confusion_matrix)

'''
for i in range (9):
	classify(i+1,2)
'''
classify(5,10)